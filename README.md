# voxl-docker

This project provides setup instructions for two docker images which provide build environments for the VOXL's ARM applications processor and Hexagon SDSP. It also provides the "voxl-docker" script for easily launching these docker images. These two docker images are prerequisites for building the majority of open-source projects on https://gitlab.com/voxl-public

For complete instructions go to https://docs.modalai.com/install-voxl-docker/